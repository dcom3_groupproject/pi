from flask import Flask, jsonify, request, render_template
from subprocess import call
import pifacedigitalio as pfio
from json import JSONEncoder
from json import JSONDecoder
#from rpi_sendor_board.temperature import main

pfio.init()

app = Flask(__name__)

alarm = 0
heat = 0
heatTemp = 21
co2Alarm = 0
fireAlarm = 0
l1 = 0
l2 = 0
l3 = 0


#Root addr will return all pi details with get request
@app.route('/', methods=['GET'])
def getAllDetails():
	
	global alarm 
	global heat
	global heatTemp 
	#global heatTemp = main()
	global co2Alarm 
	global fireAlarm 
	global l1  
	global l2 
	global l3
	piStatus = [{'alarm': alarm}, {'heating':heat}, {'CO2':co2Alarm}, {'Fire':fireAlarm}, {'light1':l1}, {'light2':l2}, {'light3':l3}, {'heatTemp': heatTemp}]
	
	response = JSONEncoder().encode(piStatus)
	return response
	

#Root addr will turn on/off alarm LED 8
@app.route('/', methods=['POST'])
def alarmCall():
	
	global alarm
	alarmStatus = request.json['name']
	
	if alarmStatus == 'On':
		if alarm == 0:
			alarm = 1
			pfio.digital_write(7, 1)
			return 'Alarm activated'
		else:
			return 'Alarm is already active'
	elif alarmStatus == 'Off':
		if alarm == 1:
			alarm = 0
			pfio.digital_write(7, 0)
			return 'Alarm de-activated'
		else:
			return 'Alarm is already de-activated'
	else:
		return 'Alarm switch error'
	

#Address with heating will turn on/off heating - LED 7
@app.route('/Heat', methods=['POST'])
def heatingCall():
	
	global heat
	heatStatus = request.json['name']
	
	if heatStatus == 'On':
		if heat == 0:
			heat = 1
			pfio.digital_write(6, 1)
			return 'Heating On'
		else:
			return 'Heating is already on'
	elif heatStatus == 'Off':
		if heat == 1:
			heat = 0
			pfio.digital_write(6, 0)
			return 'Heating Off'
		else:
			return 'Heating is already off'
	else:
		return 'Heating switch error'

	
#Turn on lights - LED 0 - 2	
@app.route('/LightOn', methods=['POST'])
def LightOn():

	global l1  
	global l2 
	global l3
	light = request.json['name']
	
	if light == 'Light 1':
		l1 = 1
		pfio.digital_write(0, 1)
		return 'Light 1 On'
		
	elif light == 'Light 2':
		l2 = 1
		pfio.digital_write(1, 1)
		return 'Light 2 On'
	
	elif light == 'Light 3':
		l3 = 1
		pfio.digital_write(2, 1)
		return 'Light 3 On'
	
	else:
		return 'Light switch error'
	
	
#Turn off lights = LED 0 - 2	
@app.route('/LightOff', methods=['POST'])
def LightOff():

	global l1  
	global l2 
	global l3
	light = request.json['name']
	
	if light == 'Light 1':
		l1 = 0
		pfio.digital_write(0, 0)
		return 'Ligh 1 Off'
		
	elif light == 'Light 2':
		l2 = 0
		pfio.digital_write(1, 0)
		return 'Light 2 Off'
	
	elif light == 'Light 3':
		l3 = 0
		pfio.digital_write(2, 0)
		return 'Light 3 Off'
	
	else:
		return 'Light switch error'
		

#Turn off all lights = LED 0 - 2	
@app.route('/LightAllOff', methods=['POST'])
def LightAllOff():

	global l1 
	global l2
	global l3
	light = request.json['name']
	
	if light == '1':
		l1 = 0
		pfio.digital_write(0, 0)
		l2 = 0
		pfio.digital_write(1, 0)
		l3 = 0
		pfio.digital_write(2, 0)
		return 'All lights off'
	else:
		return 'Light switch error'

	
if __name__ == '__main__':
	app.run(host='0.0.0.0', port=3458, debug=True)


